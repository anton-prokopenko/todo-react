
export interface ITask {
    id: string;
    name: string;
    description: string;
    priority: number;
    timeToComplete: Date;
    active: boolean;
    added: Date;
}

const delay = (ms: number) => new Promise(resolve => setTimeout(resolve, ms));

export class TaskService {
    private nextId: number = 0;
    private tasks: ITask[] = [];

    constructor() {
        for (let i=0; i<= 10; i++) {
            this.generateTask();
        }
        setInterval(() => this.generateTask(), 1000);
    }

    private generateTask(): ITask {
        const id = this.nextId++;
        let timeToComplete = new Date();
        timeToComplete.setMinutes(timeToComplete.getMinutes() + 1);
        const task = {
            id: `task-${id}`,
            name: `Task #${id}`,
            description: `Description [id=${id}] One minute task `,
            priority: 5,
            timeToComplete,
            active: true,
            added: new Date()
        };
        this.tasks.push(task);
        return task;
    }

    async fetchTasks(): Promise<ITask[]> {
        await delay(1000);
        return [...this.tasks];
    }

    async addTask(name: string, description: string, priority: number, timeToComplete: Date) {
        await delay(1000);
        const newTask = {
            name,
            description,
            priority,
            timeToComplete,
            active: true,
            id: `task-${this.nextId++}`,
            added: new Date()
        }
        this.tasks.push(newTask);
        return newTask;
    }

    async removeTask(taskId: string) {
        await delay(1000);
        const taskIndex = this.tasks.findIndex(task => task.id === taskId);
        if (taskIndex >= 0) {
            this.tasks.splice(taskIndex, 1);
        }
    }

    async completeTask(taskId: string) {
        await delay(1000);
        const task = this.tasks.find(task => task.id === taskId);
        if (task) {
            task.active = false;
            return task;
        }
    }
}
const taskService = new TaskService();

export default taskService;