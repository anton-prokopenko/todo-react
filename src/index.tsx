import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import { Provider } from 'mobx-react';
import { Applicaiton } from './stores/application';

const app = new Applicaiton();
app.init();

ReactDOM.render(
    <Provider app={app}>
        <App />
    </Provider>,
    document.getElementById('root')
);