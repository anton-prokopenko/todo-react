import { observable, action, computed, reaction } from 'mobx';
import taskService, { ITask } from '../services/taskService';
import createRouter, { Router } from 'router5';
import browserPlugin from 'router5-plugin-browser'

export interface IAppTask extends ITask {
    isUpdating?: boolean;
    isRemoving?: boolean;
    error?: string;
}

export enum Views {
    USER_SETTINGS = 'userSettings',
    TASK_LIST = 'taskList',
    NEW_TASK = 'newTask'
}

export class Applicaiton {
    tasksStore: IAppTask[] = [];
    router: Router;

    @observable filter: string = 'all';
    @observable isFetching: boolean = false;
    @observable timer: Date = new Date();
    @observable error: string = '';
    @observable viewTaskId: string | undefined;
    @observable viewTasks: IAppTask[] = [];
    @observable viewCode: string = Views.TASK_LIST;


    constructor() {
        const routes = [
            { name: Views.TASK_LIST, path: '/task-list/:filter?:id' },
            { name: Views.NEW_TASK, path: '/new-task' },
            { name: Views.USER_SETTINGS, path: '/user-settings' }
        ];
        this.router = createRouter(routes, {
            defaultRoute: Views.TASK_LIST,
            defaultParams: {
                filter: 'all'
            }
        });
        const unsubscribe = this.router.subscribe(({ route }) => {
            if (route.name === Views.USER_SETTINGS) {
                this.viewCode = Views.USER_SETTINGS;
                // todo: 
            } else if (route.name === Views.NEW_TASK) {
                this.viewCode = Views.NEW_TASK;
                // todo: 
            } else if (route.name === Views.TASK_LIST) {
                this.viewCode = Views.TASK_LIST;
                this.filter = route.params.filter;
                this.viewTaskId = route.params.id
            }
            (unsubscribe as Function)();
        });
        this.router.usePlugin(browserPlugin());
        this.router.start();

        reaction(
            () => ({ viewCode: this.viewCode, filter: this.filter, id: this.viewTaskId }),
            () => this.updateUrl()
        );

        setInterval(action(() => {
            this.timer = new Date();
        }), 1000);
    }

    async init() {
        this.fetchTasks();
    }

    updateUrl() {
        if (this.viewCode === Views.USER_SETTINGS) {
            this.router.navigate(Views.USER_SETTINGS);
            // todo:
        } else if (this.viewCode === Views.NEW_TASK) {
            this.router.navigate(Views.NEW_TASK);
            // todo:
        } else {
            this.router.navigate(Views.TASK_LIST, {
                filter: this.filter,
                id: this.viewTaskId
            });
        }
    }

    @action
    addTask(): void {
        // todo:
    }

    @action
    setViewTask(taskId: string | undefined) {
        this.viewTaskId = taskId;
    }

    @action.bound
    updateView() {
        if (this.filter === 'active') {
            this.viewTasks = this.tasksStore.filter(task => task.active);
        } else if (this.filter === 'completed') {
            this.viewTasks = this.tasksStore.filter(task => !task.active);
        } else {
            this.viewTasks = this.tasksStore;
        }
        if (this.viewTasks.findIndex(task => task.id === this.viewTaskId) < 0) {
            this.viewTaskId = 'empty';
        }
    }

    @action.bound
    fetchTasks() {
        this.isFetching = true;
        taskService
            .fetchTasks()
            .then(action(tasks => {
                this.tasksStore = tasks as ITask[];
                this.isFetching = false;
                this.error = '';
                this.updateView();
            }))
            .catch(action(err => {
                this.error = (err as Error).message;
                this.isFetching = false;
            }))
    }

    @action.bound
    filterAllTasks() {
        this.filter = 'all';
        this.updateView();
    }

    @action.bound
    filterActiveTasks() {
        this.filter = 'active';
        this.updateView();
    }

    @action.bound
    filterCompletedTasks() {
        this.filter = 'completed';
        this.updateView();
    }

    @action.bound
    completeTask(task: IAppTask) {
        task.active = false;
        task.isUpdating = true;
        taskService.completeTask(task.id)
            .then(action(() => {
                task.isUpdating = false;
                task.error = '';
                this.updateView();
            }))
            .catch(action((err: Error) => {
                console.log(err);
                task.active = true;
                task.isUpdating = false;
                task.error = err.message;
            }));
    }

    @action.bound
    removeTask(task: IAppTask) {
        task.isUpdating = true;
        task.isRemoving = true;
        taskService.removeTask(task.id)
            .then(action(() => {
                const taskIndex = this.tasksStore.findIndex(item => item.id === task.id);
                if (taskIndex >= 0) {
                    this.tasksStore.splice(taskIndex, 1);
                    this.updateView();
                }
            }))
            .catch(action((err: Error) => {
                console.log(err);
                task.isUpdating = false;
                task.isRemoving = false;
                task.error = err.message;
            }));
    }

    @computed
    public get viewTask(): IAppTask | undefined {
        return this.viewTasks.find(task => task.id === this.viewTaskId);
    }
}