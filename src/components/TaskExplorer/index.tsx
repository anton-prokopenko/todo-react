import React, { FC } from 'react';
import { IAppComponent } from '../common';
import TaskList from '../TaskList';
import Toolbar from '../Toolbar';
import TaskDetails from '../TaskDetails';

import './styles.css';
import { withApp } from '../../context';

const TaskExplorer: FC<IAppComponent> = ({ className, app }) => (
    <div className={`${className} TasksExplorer VLayout`}>
        <div className="TasksExplorer--Title VLayout--Fixed">Task List</div>
        <Toolbar className="VLayout--Fixed VMargin" />
        <TaskList className="VLayout--Stretch VMargin VMargin__bottom" />
        <TaskDetails className="VLayout--Fixed" />
    </div>
);

export default withApp(TaskExplorer);