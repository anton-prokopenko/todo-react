import React, { FC } from 'react';
import { IAppComponent } from '../common';

import './styles.css';
const TaskEditor: FC<IAppComponent> = ({className}) => (
    <div className={`${className} TaskEditor`}>
        <h1>The Task Editor</h1>
    </div>
);

export default TaskEditor;