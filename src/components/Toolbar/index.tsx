import React, { FC } from 'react';
import { IAppComponent } from '../common';
import Button from './Button';

import './styles.css';
import { withApp } from '../../context';

const Toolbar: FC<IAppComponent> = ({ className = '', app }) => (
    <div className={`${className} Toolbar HLayout`}>
        <Button
            isActive={app!.filter === 'all'}
            caption="All"
            action={app!.filterAllTasks} />
        <Button
            isActive={app!.filter === 'active'}
            caption="Active"
            action={app!.filterActiveTasks} />
        <Button
            isActive={app!.filter === 'completed'}
            caption="Completed"
            action={app!.filterCompletedTasks} />
        <div className="HLayout--Stretch"></div>
        <Button
            isActive={app!.isFetching}
            caption={app!.isFetching ? 'Fetching...' : 'Refresh'}
            action={() => !app!.isFetching && app!.fetchTasks()} />
    </div>
);

export default withApp(Toolbar);