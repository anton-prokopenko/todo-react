import { withApp } from "../../context";
import React, { FC } from "react";

export interface IToolbarButton {
    isActive: boolean;
    caption: string;
    action: any;
}

export const Button: FC<IToolbarButton> = ({ isActive, caption, action }) => {
    const activeMode = isActive ? 'Toolbar--Button__active' : '';
    return (
        <div className={`Toolbar--Button ${activeMode} HLayout--Fixed`}
            onClick={action}>
            {caption}
        </div>
    )
};

export default withApp(Button);