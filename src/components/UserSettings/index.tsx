import React, { FC } from 'react';
import { IAppComponent } from '../common';

import './styles.css';

const UserSettings: FC<IAppComponent> = ({className}) => (
    <div className={`${className} UserSettings`}>
        <h1>The User Settings</h1>
    </div>
);

export default UserSettings;