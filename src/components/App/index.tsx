import React, { FC } from 'react';
import SideBar from '../Sidebar';
import TaskExplorer from '../TaskExplorer';
import { IAppComponent } from '../common';
import { withApp } from '../../context';
import { Views } from '../../stores/application';
import TaskEditor from '../TaskEditor';
import UserSettings from '../UserSettings';

import '../layout.css';
import './styles.css';

const App: FC<IAppComponent> = ({ app }) => {
  let View: any;
  if (app!.viewCode === Views.TASK_LIST) {
    View = TaskExplorer;
  } else if (app!.viewCode === Views.NEW_TASK) {
    View = TaskEditor;
  } else if (app!.viewCode === Views.USER_SETTINGS) {
    View = UserSettings;
  } else {
    View = () => (<div>Error!!!</div>)
  }
  return (
    <div className="App HLayout">
      <SideBar className="HLayout--Fixed" />
      <View className="HLayout--Stretch" />
    </div>
  )
};

export default withApp(App);
