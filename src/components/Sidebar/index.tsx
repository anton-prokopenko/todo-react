import React, { FC } from 'react';
import { IAppComponent } from '../common';
import Button from './Button';
import { withApp } from '../../context';

import './styles.css';
import { Views } from '../../stores/application';

export const SideBar: FC<IAppComponent> = ({ className, app }) => (
    <div className={`${className} SideBar`}>
        <Button
            onClick={() => app!.viewCode = Views.NEW_TASK}
            caption="Add new task" />
        <Button
            onClick={() => app!.viewCode = Views.TASK_LIST}
            caption="Task list" />
        <Button
            onClick={() => app!.viewCode = Views.USER_SETTINGS}
            caption="User settings" />
    </div>
);

export default withApp(SideBar);