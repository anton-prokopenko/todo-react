import React, { FC } from 'react';

const Button: FC<{ onClick: any, caption: string }> = ({ onClick, caption }) => (
    <div className="SideBar--Button" onClick={onClick}>
        {caption}
    </div>
);

export default Button;