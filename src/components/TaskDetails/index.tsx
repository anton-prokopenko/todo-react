import { withApp } from "../../context";
import { IAppComponent } from "../common";
import React, { FC } from "react";

import './styles.css';

const TaskProperty: FC<{ caption: string, value?: any }> = ({caption, value}) => (
    <div className="TaskDetails--Property HLayout">
        <div className="TaskDetails--PropCaption Hlayout--Fixed">{caption}</div>
        <div className="TaskDetails--PropValue HLayout--Stretch">{value}</div>
    </div>
);

const TaskDetails: FC<IAppComponent> = ({ className, app }) => {
    const task = app!.viewTask;
    return !task
        ? <div className={`${className} TaskDetails TaskDetails__noData`}></div>
        : <div className={`${className} TaskDetails`}>
            <div className="TaskDetails--Caption">Task Details</div>
            <TaskProperty caption="Name" value={task.name} />
            <TaskProperty caption="Description" value={task.description} />
            <TaskProperty caption="Status" value={task.active ? 'Active' : 'Completed'} />
            <TaskProperty caption="Priority" value={task.priority} />
            <TaskProperty caption="Added" value={task.added.toDateString()} />
        </div>
}

export default withApp(TaskDetails);