import { withApp } from "../../context";
import { IAppComponent } from "../common";
import { IAppTask } from "../../stores/application";
import React, { FC } from "react";
import TaskAction from "./TaskAction";

export interface ITaskRow extends IAppComponent {
    task: IAppTask;
};

export const TaskRow: FC<ITaskRow> = ({ app, task }) => {
    const remainingTime = Math.max(Math.round((task.timeToComplete.getTime() - app!.timer.getTime()) / 1000), 0);
    return (
        <tr className="Tasks--TableRow" onClick={() => app!.setViewTask(task.id)}>
            <td className="Tasks--TableCell">{task.id}</td>
            <td className="Tasks--TableCell">{task.name}</td>
            <td className="Tasks--TableCell">{task.priority}</td>
            <td className="Tasks--TableCell">{remainingTime}</td>
            <td><TaskAction task={task} /></td>
        </tr>
    )
};

export default withApp(TaskRow);