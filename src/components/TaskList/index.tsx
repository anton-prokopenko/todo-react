import React, { FC } from 'react';
import { IAppComponent } from '../common';
import TaskRow from './TaskRow';
import { withApp } from '../../context';

import './styles.css';

export const TaskList: FC<IAppComponent> = ({ className = '', app }) => (
    <div className={`${className} Tasks HLayout`}>
        <table className="Tasks--Table HLayout--Stretch">
            <thead>
                <tr>
                    <th className="Tasks--TableCell">ID</th>
                    <th className="Tasks--TableCell">Name</th>
                    <th className="Tasks--TableCell">Priority</th>
                    <th className="Tasks--TableCell">Remaining Time (sec)</th>
                    <th className="Tasks--TableCell">Action</th>
                </tr>
            </thead>
            <tbody>
                {app!.viewTasks.map(task => (
                    <TaskRow key={task.id} task={task} />
                ))}
            </tbody>
        </table>
    </div>
);
export default withApp(TaskList);