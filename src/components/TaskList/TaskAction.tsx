import { withApp } from "../../context";
import { IAppComponent } from "../common";
import { IAppTask } from "../../stores/application";
import React, { FC } from "react";


export interface ITaskAction extends IAppComponent {
    task: IAppTask;
}

const TaskAction: FC<ITaskAction> = ({ app, task }) => {
    const action = task.active
        ? app!.completeTask
        : app!.removeTask;
    const caption = task.isUpdating
        ? 'Updating...'
        : task.active ? 'Complete' : 'Remove';
    const onClick = task.isUpdating
        ? undefined
        : () => action(task);
    const style = task.active
        ? 'Tasks--TaskAction__active'
        : 'Tasks--TaskAction__completed'
    return (
        <div className={`Tasks--TaskAction ${style}`} onClick={onClick}>
            {caption}
        </div>
    )
};

export default withApp(TaskAction);