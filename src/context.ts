import { observer, inject } from 'mobx-react';

export const withApp = (component: any) => inject('app')(observer(component));